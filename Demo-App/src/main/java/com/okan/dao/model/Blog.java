package com.okan.dao.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "blog")
public class Blog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "member_id")
    Integer memberId;

    @Column(name = "create_time")
    Timestamp createTime = new Timestamp(System.currentTimeMillis());

    @Column(name = "is_deleted")
    Boolean isDeleted = false;

    @Column(name = "delete_time")
    Timestamp deleteTime;
}